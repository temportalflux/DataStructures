/**
*	Author:			Dustin Yost
*	Class:			CSI-281-02
*	Assignment:		
*	Date Assigned:	09-00-2016
*	Due Date:		09-00-2016 11:00am
*
*	Description:
*		A brief description of the purpose of the program.
*
*	Certification of Authenticity:
*		I certify that this is entirely my own work, except where I have given
*		fully-documented references to the work of others. I understand the
*		definition and consequences of plagiarism and acknowledge that the
*		assessor of this assignment may, for the purpose of assessing this
*		assignment:
*			- Reproduce this assignment and provide a copy to another member
*				of academic staff; and/or
*			- Communicate a copy of this assignment to a plagiarism checking
*				service (which may then retain a copy of this assignment on
*				its database for the purpose of future plagiarism checking)
*/
